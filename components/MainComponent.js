import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator, DrawerContentScrollView, DrawerItemList, DrawerItem } from '@react-navigation/drawer';
import { View, Platform, StyleSheet, SafeAreaView, Image, Text } from 'react-native';
import Constants from 'expo-constants'; 
import { Icon } from 'react-native-elements';
import { connect } from "react-redux";
import Menu from './MenuComponent';
import DishDetail from './DishDetailComponent';
import Home from './HomeComponent';
import Contact from './ContactComponent';
import About from './AboutComponent';
import Favorites from './FavoriteComponent';
import Reservation from './ReservationComponent';
import Login from './LoginComponent';
import { fetchDishes, fetchComments, fetchPromos, fetchLeaders } from '../redux/ActionCreators';


const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const mapStateToProps = state => {
    return {
        dishes: state.dishes,
        promotions: state.promotions,
        leaders: state.leaders,
        comments: state.comments,
        favorites: state.favorites
    }
}


const mapDispatchToProps = dispatch => ({
    fetchDishes: () => dispatch(fetchDishes()),
    fetchComments: () => dispatch(fetchComments()),
    fetchPromos: () => dispatch(fetchPromos()),
    fetchLeaders: () => dispatch(fetchLeaders()),
});

function LoginMenu() {
    return (
        <Stack.Navigator
            screenOptions={({ navigation }) => ({
                gestureEnabled: true,
                headerStyle: [styles.headerColor],
                headerTitleStyle: [styles.headerTitle],
                headerTintColor: [styles.headerTint],
                headerBackTitleVisible: false,
                headerLeft: () => (
                    <Icon name='menu' style={styles.navIcon} onPress={() => navigation.toggleDrawer()} />
                )
            })}
            headerMode="float"
        >
            <Stack.Screen name="Login" component={Login} options={{ title: 'Login' }} />
        </Stack.Navigator>
    );
}

function MenuNavigator({ dishes , comments}) {
    return (
        <Stack.Navigator
            screenOptions={{
                gestureEnabled: true,
                headerStyle: [styles.headerColor],
                headerTitleStyle: [styles.headerTitle],
                headerTintColor: [styles.headerTint],
                headerBackTitleVisible: false
            }}
            headerMode="float"
        >
            <Stack.Screen name="Menu"
                options={({ navigation }) => ({
                    title: 'menu',
                    headerLeft: () => (
                        <Icon  name= 'menu' style= {styles.navIcon} onPress={() => navigation.toggleDrawer()} />
                    )
                  })}
            >
                {props => <Menu
                    {...props} dishes={dishes}
                />}
            </Stack.Screen>

            <Stack.Screen name="DishDetail"
                options={({ route }) => ({
                    title: route.params.selectedDish.name
                })}
            >
                {props => <DishDetail
                    {...props} dishes={dishes} comments={comments}
                />}
            </Stack.Screen>
        </Stack.Navigator>
    );
}

function FavoriteNavigator({ dishes, comments, favorites }) {
    return (
        <Stack.Navigator
            screenOptions={{
                gestureEnabled: true,
                headerStyle: [styles.headerColor],
                headerTitleStyle: [styles.headerTitle],
                headerTintColor: [styles.headerTint],
                headerBackTitleVisible: false
            }}
            headerMode="float"
        >
            <Stack.Screen name="My Favorites"
                options={({ navigation }) => ({
                    title: 'My Favorites',
                    headerLeft: () => (
                        <Icon name='menu' style={styles.navIcon} onPress={() => navigation.toggleDrawer()} />
                    )
                })}
            >
                {props => <Favorites
                    {...props} dishes={dishes} favorites={favorites}
                />}
            </Stack.Screen>

            <Stack.Screen name="DishDetail"
                options={({ route }) => ({
                    title: route.params.selectedDish.name
                })}
            >
                {props => <DishDetail
                    {...props} dishes={dishes} comments={comments} 
                />}
            </Stack.Screen>
        </Stack.Navigator>
    );
}

function HomeMenu({ dishes, promotions, leaders }) {
    return (
        <Stack.Navigator
            screenOptions={({ navigation }) => ({
                gestureEnabled: true,
                headerStyle: [styles.headerColor],
                headerTitleStyle: [styles.headerTitle],
                headerTintColor: [styles.headerTint],
                headerBackTitleVisible: false,
                headerLeft: () => (
                    <Icon  name= 'menu' style= {styles.navIcon} onPress={() => navigation.toggleDrawer()} />
                )
            })}
            headerMode="float"
        >
            <Stack.Screen name="Home" options={{title: 'Home' }} >
                {props => <Home
                    {...props} dishes={dishes}
                    leaders={leaders}
                    promotions={promotions}
                    
                />}
            </Stack.Screen>

        </Stack.Navigator>
    );
}

function ContactMenu() {
    return (
        <Stack.Navigator
            screenOptions={({ navigation }) => ({
                gestureEnabled: true,
                headerStyle: [styles.headerColor],
                headerTitleStyle: [styles.headerTitle],
                headerTintColor: [styles.headerTint],
                headerBackTitleVisible: false,
                headerLeft: () => (
                    <Icon  name= 'menu' style= {styles.navIcon} onPress={() => navigation.toggleDrawer()} />
                )
            })}
            headerMode="float"
        >
            <Stack.Screen name="Contact" component={Contact} options={{  title: 'Contact Us' }}/>
        </Stack.Navigator>
    );
}

function ReservationNavigator() {
    return (
        <Stack.Navigator
            screenOptions={({ navigation }) => ({
                gestureEnabled: true,
                headerStyle: [styles.headerColor],
                headerTitleStyle: [styles.headerTitle],
                headerTintColor: [styles.headerTint],
                headerBackTitleVisible: false,
                headerLeft: () => (
                    <Icon name='menu' style={styles.navIcon} onPress={() => navigation.toggleDrawer()} />
                )
            })}
            headerMode="float"
        >
            <Stack.Screen name="Reservation" component={Reservation} options={{ title: 'Reserve Table' }} />
        </Stack.Navigator>
    );
}

function AboutMenu({ leaders}) {
    return (
        <Stack.Navigator
            screenOptions={({ navigation }) => ({
                gestureEnabled: true,
                headerStyle: [styles.headerColor],
                headerTitleStyle: [styles.headerTitle],
                headerTintColor: [styles.headerTint],
                headerBackTitleVisible: false,
                headerLeft: () => (
                    <Icon  name= 'menu' style= {styles.navIcon} onPress={() => navigation.toggleDrawer()} />
                )
            })}
            headerMode="float"
        >
            <Stack.Screen name="About" options={{ title: 'About Us' }} >
                {props => <About
                    {...props} leaders={leaders}
                />}
            </Stack.Screen>
        </Stack.Navigator>
    );
}

const CustomDrawerContent = (props) => {
    return(
        <DrawerContentScrollView {...props}>
            <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}> 
                <View style={styles.drawerHeader}>
                    <View style={{flex:1}}>
                        <Image source={require('./images/logo.png')} style={styles.drawerImage} />
                    </View>
                    <View style={{flex:2}}>
                        <Text style={styles.drawerHeaderText}>Ristorante Con Fusion</Text>
                    </View>
                </View>
            </SafeAreaView>
            <DrawerItemList {...props} />
        </DrawerContentScrollView>
    )
}

function MainMenu({ dishes, promotions, leaders, comments, favorites }) {
        return (
            <Drawer.Navigator
                drawerStyle={{
                    backgroundColor: "#D1C4E9"
                }}
                headerMode="float"
                drawerContent={props => <CustomDrawerContent {...props} />}
            >
                <Drawer.Screen name="Home" options={
                    { title: 'Home',
                     drawerLabel: 'Home',
                     drawerIcon: ({tintColor})=> (
                         <Icon name='home' 
                            type='font-awesome'
                            style= {[styles.icon, {tintColor: tintColor}]}
                        />
                     )
                    }
                } >
                    {props => <HomeMenu
                        {...props} dishes={dishes}
                        leaders={leaders}
                        promotions={promotions}
                    />}
                </Drawer.Screen>


                <Drawer.Screen name="Login" options={
                    {
                        title: 'Login',
                        drawerLabel: 'Login',
                        drawerIcon: ({ tintColor }) => (
                            <Icon name='address-card'
                                type='font-awesome'
                                color={tintColor}
                                style={[styles.icon]}
                            />
                        )
                    }
                } component={LoginMenu} />

                
                <Drawer.Screen name="About" options={
                    { title: 'About Us', 
                        drawerLabel: 'About Us',
                        drawerIcon: ({tintColor})=> (
                            <Icon name='info-circle' 
                               type='font-awesome'
                               color= {tintColor}
                               style= {[styles.icon]}
                           />
                        ) 
                    }
                } >
                    {props => <AboutMenu
                        {...props} leaders={leaders}
                    />}
                </Drawer.Screen>
                <Drawer.Screen name="Menu" options={
                    { title: 'Menu', 
                        drawerLabel: 'Menu',
                        drawerIcon: ({tintColor})=> (
                            <Icon name='list' 
                               type='font-awesome'
                               color= {tintColor}
                               style= {[styles.icon]}
                           />
                        ) 
                    }
                } >
                    {props => <MenuNavigator
                        {...props}  
                        dishes={dishes}
                        comments={comments}
                    />}
                </Drawer.Screen>

                <Drawer.Screen name="Favorite" options={
                    {
                        title: 'My Favorite',
                        drawerLabel: 'My Favorite',
                        drawerIcon: ({ tintColor }) => (
                            <Icon name='heart'
                                type='font-awesome'
                                color={tintColor}
                                style={[styles.icon]}
                            />
                        )
                    }
                } >
                    {props => <FavoriteNavigator
                        {...props}
                        dishes={dishes}
                        comments={comments}
                        favorites={favorites}
                    />}
                </Drawer.Screen>

                <Drawer.Screen name="Reservation" options={
                    {
                        title: 'Reserve Table',
                        drawerLabel: 'Reserve Table',
                        drawerIcon: ({ tintColor, focused }) => (
                            <Icon name='cutlery'
                                type='font-awesome'
                                color={tintColor}
                                style={[styles.icon]}
                            />
                        )
                    }
                } component={ReservationNavigator} />

                <Drawer.Screen name="Contact" options={
                    { title: 'Contact Us', 
                        drawerLabel: 'Contact Us',
                        drawerIcon: ({tintColor})=> (
                            <Icon name='sign-in' 
                               type='font-awesome'
                               color= {tintColor}
                               style= {[styles.icon]}
                           />
                        ) 
                    }
                } component={ContactMenu} />
            </Drawer.Navigator>
        );
}

class Main extends React.Component {

    componentDidMount() {
        this.props.fetchDishes();
        this.props.fetchPromos();
        this.props.fetchComments();
        this.props.fetchLeaders();
    }

    render() {
        return (
            <View style={styles.container}>
                <NavigationContainer>
                    <MainMenu dishes={this.props.dishes}
                        comments={this.props.comments}
                        leaders={this.props.leaders}
                        promotions={this.props.promotions}
                        favorites={this.props.favorites}
                    />
                </NavigationContainer>
            </View>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);

const styles = StyleSheet.create({
    container: {
        marginTop: Platform.OS === 'ios' ? 0 : Constants.statusBarHeight
    },
    headerColor: {
        backgroundColor: "#512DA8"
    },
    headerTitle: {
        color: "#fff"
    },
    headerTint: {
        backgroundColor: "#ddd"
    },
    icon: {
      width: 24,
      height: 24,
    },
    navIcon: {
        width: 26,
        height: 26,
        marginLeft: 15,
        color:'#ffffff',
        fontSize: 26,
        backgroundColor: "#ccc"
    },
  container: {
    flex: 1,
  },
  drawerHeader: {
    backgroundColor: '#512DA8',
    height: 140,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  drawerHeaderText: {
    color: '#ffffff',
    fontSize: 24,
    fontWeight: 'bold'
  },
  drawerImage: {
    margin: 10,
    width: 80,
    height: 60
  }
});
