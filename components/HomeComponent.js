import React from 'react';
import { View, Text, ScrollView, Image, Animated, Easing} from 'react-native';
import { Card } from "react-native-elements";
import {Asset} from 'expo-asset';
import baseUrl from '../shared/baseUrl';
import { Loading } from './LoadingComponent';




//const imageURI = Asset.fromModule(require('./images/vadonut.png')).uri;

function RenderList(props) {
    const item = props.item;
    if (props.isLoading) {
        return (
            <Card>
                <Loading />
            </Card>
        )
    }

    else if (props.errmsg) {
        return (
            <Card>
                <Text style={{ margin: 10 }}>{props.errmsg}</Text>
            </Card>
        )
    }

    else {
        if (item != null) {
            return (
                <Card>
                    <Card.Title>{item.name}</Card.Title>
                    <Card.Image source={{ uri: baseUrl + item.image }}  ></Card.Image>
                    <Text h3 style={{ margin: 10, textAlign: 'center' }}>{item.designation} </Text>
                    <Card.Divider />
                    <Text style={{ margin: 10 }}>

                        {item.description}
                    </Text>
                </Card>
            )
        } else {
            return (<View></View>)
        }
    }
}

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.animatedValue = new Animated.Value(0);
    }

    componentDidMount() {
        this.animate()
    }

    animate() {
        this.animatedValue.setValue(0)
        Animated.timing(
            this.animatedValue,
            {
                toValue: 8,
                duration: 8000,
                easing: Easing.linear
            }
        ).start(() => this.animate())
    }

    render() {
        const xpos1 = this.animatedValue.interpolate({
            inputRange: [0, 1, 3, 5, 8],
            outputRange: [1200, 600, 0, -600, -1200]
        })
        const xpos2 = this.animatedValue.interpolate({
            inputRange: [0, 2, 4, 6, 8],
            outputRange: [1200, 600, 0, -600, -1200]
        })
        const xpos3 = this.animatedValue.interpolate({
            inputRange: [0, 3, 5, 7, 8],
            outputRange: [1200, 600, 0, -600, -1200]
        })

        return (
            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
                <Animated.View style={{ width: '100%', transform: [{ translateX: xpos1 }] }}>
                    <RenderList item={this.props.dishes.dishes.filter((dish) => dish.featured)[0]}
                        isLoading={this.props.dishes.isLoading}
                        errmsg={this.props.dishes.errmsg}
                    />
                </Animated.View>
                <Animated.View style={{ width: '100%', transform: [{ translateX: xpos2 }] }}>
                <RenderList item={this.props.promotions.promotions.filter((promo) => promo.featured)[0]}
                    isLoading={this.props.promotions.isLoading}
                        errmsg={this.props.promotions.errmsg} />

                </Animated.View>
                <Animated.View style={{ width: '100%', transform: [{ translateX: xpos3 }] }}>
                <RenderList item={this.props.leaders.leaders.filter((leader) => leader.featured)[0]}
                    isLoading={this.props.leaders.isLoading}
                        errmsg={this.props.leaders.errmsg} />

                </Animated.View>
            </View>
        );
    }
            
}


export default Home;