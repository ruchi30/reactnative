import React from 'react';
import { ScrollView, Text, View, FlatList} from 'react-native';
import { Card, ListItem, Avatar } from "react-native-elements";
import baseUrl from '../shared/baseUrl';
import { Loading } from './LoadingComponent';
import * as Animatable from 'react-native-animatable';


function History() {
    return (
        <Card>
            <Card.Title>Our History</Card.Title>
            <Card.Divider />
            <Text>
                Started in 2010, Ristorante con Fusion quickly established itself as a culinary
                icon par excellence in Hong Kong. With its unique brand of world fusion cuisine that
                can be found nowhere else, it enjoys patronage from the A-list clientele in Hong Kong.
                Featuring four of the best three-star Michelin chefs in the world,
                you never know what will arrive on your plate the next time you visit us.
                </Text>

            <Text>
                The restaurant traces its humble beginnings to The Frying Pan, a successful chain
                started by our CEO, Mr. Peter Pan, that featured for the first time the world's best
                cuisines in a pan.
                </Text>
        </Card>
       )
}

export default function About(props) {
    const renderLeaderItem = ({ item, index }) => {
        return (
            <ListItem bottomDivider key={index} Chevron={false}>
                <Avatar rounded
                    source={{ uri: baseUrl + item.image}}>
                </Avatar>
                <ListItem.Content>
                    <ListItem.Title>{item.name}</ListItem.Title>
                    <ListItem.Subtitle>{item.designation}, {item.abbr}</ListItem.Subtitle>
                    <Text>{item.description}</Text>
                </ListItem.Content>
            </ListItem>
        )
    }

    if (props.leaders.isLoading) {
        return (
            <ScrollView>
                <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
                    <History />
                    <Card>
                        <Card.Title>Corporate Leadership</Card.Title>
                        <Card.Divider />
                        <Loading />
                    </Card>
                </Animatable.View>
            </ScrollView>
        )
    }

    else if (props.leaders.errmsg) {
        return (
            <ScrollView>
                <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
                    <History />
                    <Card>
                        <Card.Title>Corporate Leadership</Card.Title>
                        <Card.Divider />
                        {props.leaders.errmsg}
                    </Card>
                </Animatable.View>
            </ScrollView>  
        )
    }

    else {
        return (
            <ScrollView>
                <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
                    <History />
                    <Card>
                        <Card.Title>Corporate Leadership</Card.Title>
                        <Card.Divider />
                        <FlatList
                            data={props.leaders.leaders}
                            renderItem={renderLeaderItem}
                            keyExtractor={item => item.id.toString()}
                        />
                    </Card>
                </Animatable.View>
            </ScrollView>  
        )
    }
}
