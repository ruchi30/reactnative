import React, { Component, useRef }  from "react";
import { View, Text, ScrollView, FlatList, Modal, StyleSheet, Button, Alert, PanResponder } from 'react-native';
import { Card, Icon, Rating, Input } from "react-native-elements";
import baseUrl from '../shared/baseUrl';
import { connect } from "react-redux";
import { postFavorite, postComment } from '../redux/ActionCreators';
import * as Animatable from 'react-native-animatable';


const mapStateToProps = state => {
    return {
        favorites : state.favorites
    }
}
const mapDispatchToProps = dispatch => ({
    postFavorite: (dishId) => dispatch(postFavorite(dishId)),
    postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment))
})

function RenderComment(props) {
    const comments = props.comments;

    if (comments != null) {
        const rendercommentitem = ({ item, index }) => {
            return (
                <View key={index} style={{ margin: 10 }}>
                    <Text style={{ fontSize: 14 }}>{item.comment}</Text>
                    <Text style={{ fontSize: 12 }}>{item.rating} Stars</Text>
                    <Text style={{ fontSize: 12 }}>{'-- ' + item.author + ', ' + item.date} </Text>
                </View>
            )
        }
        return (
            <Card>
                <Card.Title>Comments</Card.Title>
                <Card.Divider />
                <FlatList
                    data={comments}
                    renderItem={rendercommentitem}
                    keyExtractor={item => item.id.toString()}
                />
            </Card>
        )
    } else {
        return (<View></View>)
    }
}

function RenderDish(props) {
    const dish = props.dish;
    const handleViewRef = useRef(null);

    const recognizeDrag = ({ moveX, moveY, dx, dy }) => {
        // moveX = x coordinate of the most recent touch
        // moveY = y coordinate of the most recent touch
        // dx = accumulated x distance since first touch to the latest touch
        // dy = accumulated y distance since first touch to the latest touch

        return dx < -200 ? true : false;
    }

    const recognizeComment = ({ moveX, moveY, dx, dy }) => {
        return dx > 200 ? true : false;
    }

    const panResponder = PanResponder.create({
        onStartShouldSetPanResponder: (e, gestureState) => {
            return true;
        },
        onPanResponderGrant: () => {
            handleViewRef.current.rubberBand(1000)
                .then(endState => console.log(endState.finished ? 'finished' : 'cancelled'))
        },
        onPanResponderEnd: (e, gestureState) => {
            if (recognizeDrag(gestureState)) {
                Alert.alert(
                    'Add to favorites ?',
                    `Are you sure you wish to add ${dish.name} to your favorites ?`,
                    [
                        {
                            text: 'Cancel',
                            onPress: () => console.log(`${dish.name} not added to favorite`),
                            style: 'cancel'
                        },
                        {
                            text: 'Ok',
                            onPress: () => props.favorite ? console.log('already favorite') : props.onFavoritePress()
                        }
                    ],
                    {
                        cancelable: false
                    }
                )
            }

            if (recognizeComment(gestureState)) {
                props.onPressAddComment();
            }

            return true;
        }
    });

    if (dish != null) {
        return (

            <Animatable.View animation="fadeInDown" duration={2000} delay={1000} ref={handleViewRef}
                // Adding panHandlers to View
                {...panResponder.panHandlers}>
                <Card>
                    <Card.FeaturedTitle>{dish.name}</Card.FeaturedTitle>
                    <Card.Image source={{ uri: baseUrl + dish.image }}  ></Card.Image>
                    <Card.Divider />
                    <Text style={{ margin: 10 }}>
                        {dish.description}
                    </Text>
                    <View style={styles.icons}>
                        <Icon
                            raised
                            reverse
                            name={props.favourate ? 'heart' : 'heart-o'}
                            type="font-awesome"
                            color="#f50"
                            //onPress={() => props.favourate ? console.log("Already Added") : props.addFev()}
                            onPress={() => props.favorite ? console.log('Already favorite') : props.onPress()}
                        />
                        <Icon
                            raised
                            reverse
                            name="pencil"
                            type="font-awesome"
                            color="#512DA8"
                            onPress={props.onPressAddComment}
                        />
                    </View>
                </Card>
            </Animatable.View>
        )
    } else {
        return (<View></View>)
    }
}

class DishDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            author: "",
            comment: "",
            rating: 5,
            dishId: props.route.params.selectedDish.id,
        };
    }



    markFavourate() {
        const { rating, author, comment, dishId } = this.state;
        this.props.postFavorite(dishId);
    }

    toggleCommentModal = () => {
        this.setState({ showModal: !this.state.showModal });
    };

    ratingCompleted = rating => {
        this.setState({ rating });
    };

    handleAuthorInput = author => {
        this.setState({ author });
    };

    handleCommentInput = comment => {
        this.setState({ comment });
    };

    resetForm() {
        this.setState(DishDetail.defaultState());
    }

    handleComment() {
        const { rating, author, comment, dishId } = this.state;
        //const dishId = this.props.navigation.getParam("selectedDish.id", "");
        this.toggleCommentModal();
        this.props.postComment(dishId, rating, author, comment);
    }



    render() {
        //const { route } = this.props;
        //const { selectedDish } = route.params;
        return (
            <ScrollView>
                <RenderDish dish={this.props.dishes.dishes[this.state.dishId]}
                    favourate={this.props.favorites.some(el => el === this.state.dishId)}
                    onPressAddComment={this.toggleCommentModal}
                    onPress={() => this.markFavourate(this.state.dishId)}
                    />
                <Animatable.View animation="fadeInDown" duration={2000} delay={1000}>
                    <RenderComment comments={this.props.comments.comments.filter((comment) => this.state.dishId == comment.dishId)} />
                </Animatable.View>

                <Modal
                    animationType={"slide"}
                    transparent={false}
                    visible={this.state.showModal}
                    onDismiss={() => this.toggleCommentModal()}
                    onRequestClose={() => this.toggleCommentModal()}
                >
                    <View style={styles.modal}>
                        <Rating
                            imageSize={30}
                            startingValue={5}
                            showRating
                            onFinishRating={this.ratingCompleted}
                            style={{ paddingVertical: 10 }}
                        />
                        <Input
                            placeholder="Author"
                            onChangeText={this.handleAuthorInput}
                            leftIcon={{ type: "font-awesome", name: "user-o" }}
                        />
                        <Input
                            placeholder="Comment"
                            onChangeText={this.handleCommentInput}
                            leftIcon={{ type: "font-awesome", name: "comment-o" }}
                        />
                        <View style={{ margin: 10 }}>
                            <Button
                                onPress={() => {
                                    this.handleComment();
                                    this.resetForm();
                                }}
                                color="#512DA8"
                                title="Submit"
                            />
                        </View>
                        <View style={{ margin: 10 }}>
                            <Button
                                onPress={() => {
                                    this.toggleCommentModal();
                                    this.resetForm();
                                }}
                                color="gray"
                                title="Cancel"
                            />
                        </View>
                    </View>
                </Modal>
            </ScrollView>
        )
    }

}

const styles = StyleSheet.create({
    modal: {
        justifyContent: "center",
        margin: 20
    },
    modalTitle: {
        fontSize: 24,
        fontWeight: "bold",
        backgroundColor: "#512DA8",
        textAlign: "center",
        color: "white",
        marginBottom: 20
    },
    modalText: {
        fontSize: 18,
        margin: 10
    },
    icons: {
        alignItems: "center",
        justifyContent: "center",
        flex: 1,
        flexDirection: "row"
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(DishDetail);