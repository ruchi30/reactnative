import React from 'react';
import { View, FlatList, Text } from 'react-native';
import { ListItem, Avatar, Card } from "react-native-elements";
import {Asset} from 'expo-asset';
import baseUrl from '../shared/baseUrl';
import { Loading } from './LoadingComponent';
import * as Animatable from 'react-native-animatable';
//const imageURI = Asset.fromModule(require('./images/vadonut.png')).uri;


export default function Menu(props) {
    const { navigation } = props;
    const renderMenuItem = ({ item, index }) => {
        return (
            <ListItem bottomDivider
                key={index}
                onPress={() => navigation.navigate('DishDetail', { selectedDish: item })}
            >
                <Avatar rounded
                    Image source={{ uri: baseUrl + item.image}}>
                </Avatar>
                <ListItem.Content>
                    <ListItem.Title>{item.name}</ListItem.Title>
                    <ListItem.Subtitle>{item.description}</ListItem.Subtitle>
                </ListItem.Content>
                <ListItem.Chevron />
            </ListItem>
        );
    }

    if (props.dishes.isLoading) {
        return (
            <Card>
                <Loading />
            </Card>
        )
    }

    else if (props.dishes.errmsg) {
        return (
            <Card>
                <Text style={{ margin: 10 }}>{props.dishes.errmsg}</Text>
            </Card>
        )
    }

    else {
        return (
            <Animatable.View animation="fadeInRightBig" duration={2000}> 
                <FlatList
                    data={props.dishes.dishes}
                    renderItem={renderMenuItem}
                    keyExtractor={item => item.id.toString()}
                />
            </Animatable.View>
        );
    }

    
}
