import React from 'react';
import { View, FlatList, Text, TouchableOpacity, Alert } from 'react-native';
import { ListItem, Avatar, Card } from "react-native-elements";
import baseUrl from '../shared/baseUrl';
import { connect } from "react-redux";
import { Loading } from './LoadingComponent';
import { deleteFavorite } from '../redux/ActionCreators';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import * as Animatable from 'react-native-animatable';


const mapDispatchToProps = dispatch => ({
    deleteFavorite: (dishId) => dispatch(deleteFavorite(dishId))
})

class Favorites extends React.Component {
    render() {
        //const { navigate } = this.props.navigation;
        const { navigation } = this.props;
        const renderMenuItem = ({ item, index }) => {
            const rightButton = (progress, dragX) => {
                const scale = dragX.interpolate({
                    inputRange: [-100, 0],
                    outputRange: [0.7, 0]
                })
                return (
                    <>
                        <TouchableOpacity
                            //onPress={() => this.props.deleteFavorite(item.id)}
                            onPress={() => {
                                Alert.alert(
                                    'Delete Favorite?',
                                    'Are you sure you wish to delete the favorite dish ' + item.name + '?',
                                    [
                                        {
                                            text: 'Cancel',
                                            onPress: () => console.log(item.name + 'Not Deleted'),
                                            style: ' cancel'
                                        },
                                        {
                                            text: 'OK',
                                            onPress: () => this.props.deleteFavorite(item.id)
                                        }
                                    ],
                                    { cancelable: false }
                                )
                            }}
                        
                        >
                            <View style={{ flex: 1, backgroundColor: 'red', justifyContent: 'center' }}>
                                <Text style={{
                                    color: 'white', paddingHorizontal: 10,
                                    fontWeight: '600', transform: [{ scale }]
                                }}>Delete</Text>
                            </View>
                            
                        </TouchableOpacity>
                    </>
                );
            }
            return (
                <Swipeable renderRightActions={rightButton}>
                    <ListItem bottomDivider
                        key={index}
                        //th7onPress={() => navigation.navigate('DishDetail', { selectedDish: item })}
                    >
                        <Avatar rounded
                            Image source={{ uri: baseUrl + item.image }}>
                        </Avatar>
                        <ListItem.Content>
                            <ListItem.Title>{item.name}</ListItem.Title>
                            <ListItem.Subtitle>{item.description}</ListItem.Subtitle>
                        </ListItem.Content>
                        <ListItem.Chevron />
                    </ListItem>
                </Swipeable>
            );
        }

        if (this.props.dishes.isLoading) {
            return (
                <Card>
                    <Loading />
                </Card>
            )
        }

        else if (this.props.dishes.errmsg) {
            return (
                <Card>
                    <Text style={{ margin: 10 }}>{props.dishes.errmsg}</Text>
                </Card>
            )
        }

        else {
            return (
                <Animatable.View animation="fadeInRightBig" duration={2000}>   
                    <FlatList
                        data={this.props.dishes.dishes.filter(dish => this.props.favorites.some(el => el === dish.id))}
                        renderItem={renderMenuItem}
                        keyExtractor={item => item.id.toString()}
                    />
                </Animatable.View>
          );
        }
    }
}

export default connect(mapDispatchToProps)(Favorites);

