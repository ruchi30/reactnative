import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { Dishes } from './dishes';
import { Leaders } from './leaders';
import { Comments } from './comments';
import { Promotions } from './promotions';
import { favorites } from './favorites';
import { persistStore, persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/es/storage';

const config = {
	key: 'root',
	storage,
	debug: true
}

export const configurestore = () => {
	const store = createStore(
		persistCombineReducers(config,{
			dishes: Dishes,
			leaders: Leaders,
			comments: Comments,
			promotions: Promotions,
			favorites: favorites
		}),
		applyMiddleware( thunk, logger)
	)

	const persistor = persistStore(store)

	return { persistor, store };
}